/**
 * Specify required object
 * THIS TEMPLATE HAS BEEN DEPRECATED AND WILL BE REMOVED IN THE FUTURE
 * @examples require(".").sampleData
 */
export interface IModel {
  username: string;
  returnUrl: string;
}

export const sampleData: IModel[] = [
  {
    username: 'test@rocketmakers.com',
    returnUrl: 'https://www.rocketmakers.com',
  },
  {
    username: 'test2@rocketmakers.com',
    returnUrl: 'https://www.rocketmakers.com',
  },
];
