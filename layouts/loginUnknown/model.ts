/**
 * Specify required object
 * @examples require(".").sampleData
 */
export interface IModel {
  email: string
  returnUrl: string
}

export const sampleData: IModel[] = [
  {
    email: "test@example.com",
    returnUrl: "https://www.rocketmakers.com",
  },
  {
    email: "test@example.com",
    returnUrl: "https://www.rocketmakers.com",
  },
]
