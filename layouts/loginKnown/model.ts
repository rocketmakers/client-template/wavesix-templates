/**
 * Specify required object
 * @examples require(".").sampleData
 */
export interface IModel {
  displayName: string
  returnUrl: string
}

export const sampleData: IModel[] = [
  {
    displayName: "John",
    returnUrl: "https://www.rocketmakers.com",
  },
  {
    displayName: "Mandy",
    returnUrl: "https://www.rocketmakers.com",
  },
]
